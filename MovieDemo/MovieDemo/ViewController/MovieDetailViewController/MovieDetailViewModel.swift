//
//  MovieDetailViewModel.swift
//  MovieDemo
//
//  Created by Jazlly on 07/06/2021.
//

import UIKit

class MovieDetailViewModel {
    
    var changeHandler: ((BaseViewModelChange) -> Void)?
    let networkServiceCall = NetworkServiceCalls()

    var movieObject : MoviewResults?
    var movieDetails : GetMovieDetailsResponse?
    
    func startSynching() {
        emit(.loaderStart)
        self.networkServiceCall.getMovieDetails(movieID: self.movieObject?.id ?? 0 ) { (state) in
            self.emit(.loaderEnd)
            self.movieDetails = LocalData.shared.getMovieDetails(movieID: self.movieObject?.id ?? 0)

            switch state {
            case .success( let result as GetMovieDetailsResponse):
                print(result.title ?? "")
                LocalData.shared.setMoviewDetails(result)
                self.movieDetails = LocalData.shared.getMovieDetails(movieID: result.id ?? 0)
                self.emit(.updateDataModel)

            case .failure(let error):
                self.emit(.loaderEnd)
                self.emit(.error(message: error))
                
            default:break
            }
        }
    }
    
    func emit(_ change: BaseViewModelChange) {
             changeHandler?(change)
    }

}
