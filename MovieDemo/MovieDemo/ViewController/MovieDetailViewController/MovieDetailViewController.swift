//
//  MovieDetailViewController.swift
//  MovieDemo
//
//  Created by Jazlly on 07/06/2021.
//

import UIKit

class MovieDetailViewController: BaseViewController,BaseViewControllerProtocol {
   
    @IBOutlet weak var mainView: UIView!

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelVoteAvegrage: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelVoteCount: UILabel!
    @IBOutlet weak var labelTag: UILabel!
    @IBOutlet weak var labelStatus: UILabel!

    @IBOutlet weak var movieImageView: UIImageView!

    
    var viewModel : MovieDetailViewModel?
    var movieObject :MoviewResults?

    static func storyboardInstance() -> MovieDetailViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.bindViewModel()
        self.setupUI()
    }
    
    //MARK: ViewModel-
    func bindViewModel() {
        
        self.viewModel = MovieDetailViewModel()
        self.viewModel?.changeHandler = {
            [unowned self] change in
            switch change {
            case .error(let errorMessage):
                // Error Pop-Up
                print(errorMessage)
                self.showAlert(errorMessage)
                break
            case .loaderEnd:
                self.removeSpinner()
                break
            case .loaderStart:
                self.showSpinner(onView: self.view)
                break
            case .updateDataModel:
                // BindData
                self.updateDataIfNeeded()
                
                break
            }
        }
        
    }

    func setupUI() {
        self.setBackBarItem(isNative: false)
        self.viewModel?.movieObject = self.movieObject
        self.viewModel?.startSynching()
        self.title = self.viewModel?.movieObject?.title ?? ""
        self.setShadowView(view: self.mainView, color: .darkGray)
        self.setShadowView(view: self.movieImageView, color: .lightGray)

    }

    fileprivate func updateDataIfNeeded() {
        
        if let releaseDate = self.viewModel?.movieDetails?.release_date, releaseDate.count > 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let currentDate = dateFormatter.date(from: releaseDate) ?? Date()
            dateFormatter.dateFormat =  "E d MMM yyyy"
            let currentDates = dateFormatter.string(from: currentDate)
            self.labelDate.text = "Release Date: \(currentDates)"

        } else {
            self.labelDate.text = "Release Date: \(self.viewModel?.movieDetails?.release_date ?? "")"
        }

        self.labelTitle.text = self.viewModel?.movieDetails?.title ?? ""
        self.labelDescription.text = self.viewModel?.movieDetails?.overview ?? ""
        self.labelStatus.text = "Status:\(self.viewModel?.movieDetails?.status ?? "")"
        self.labelTag.text = self.viewModel?.movieDetails?.tagline ?? ""

        self.labelVoteAvegrage.text = "Vote Average: \(self.viewModel?.movieDetails?.vote_average ?? 0)"
        self.labelVoteCount.text = "Vote: \(self.viewModel?.movieDetails?.vote_count ?? 0)"
        self.movieImageView.downloadImageFrom(link: self.viewModel?.movieDetails?.backdrop_path ?? "",placeHolder: "placeholder",mode: .scaleAspectFill)

    }
    
    func setShadowView(view: UIView, color: UIColor) {
        view.layer.cornerRadius = 5
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 3.0
        view.layer.shadowOpacity = 0.5
        view.layer.masksToBounds =  false
    }

}
