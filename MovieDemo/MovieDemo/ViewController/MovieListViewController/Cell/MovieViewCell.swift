//
//  MovieViewCell.swift
//  MovieDemo
//
//  Created by RK on 07/06/2021.
//

import UIKit

class MovieViewCell: BaseViewCell {
    
    @IBOutlet weak var mainView: UIView!

    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelVoteAvegrage: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelVoteCount: UILabel!
    
    @IBOutlet weak var movieImageView: UIImageView!
    
    var movideData : MoviewResults?{
        didSet {
            guard let movie = movideData else { return }
            
            if let releaseDate = movie.release_date, releaseDate.count > 0{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let currentDate = dateFormatter.date(from: releaseDate) ?? Date()
                dateFormatter.dateFormat =  "E d MMM yyyy"
                let currentDates = dateFormatter.string(from: currentDate)
                self.labelDate.text = "Release Date: \(currentDates)"

            } else {
                self.labelDate.text = "Release Date: \(movie.release_date ?? "")"

            }
            self.labelTitle.text = movie.title ?? ""
            self.labelDescription.text = movie.overview ?? ""
            
            self.labelVoteAvegrage.text = "Vote Average: \(movie.vote_average ?? 0)"
            self.labelVoteCount.text = "Vote: \(movie.vote_count ?? 0)"
            self.movieImageView.downloadImageFrom(link: movie.backdrop_path ?? "",placeHolder: "placeholder",mode: .scaleAspectFill)

        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setShadowView(view: self.mainView, color: .darkGray)
        self.setShadowView(view: self.movieImageView, color: .lightGray)

    }
    
    func setShadowView(view: UIView, color: UIColor) {
        view.layer.cornerRadius = 5
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 3.0
        view.layer.shadowOpacity = 0.5
        view.layer.masksToBounds =  false
    }

}
