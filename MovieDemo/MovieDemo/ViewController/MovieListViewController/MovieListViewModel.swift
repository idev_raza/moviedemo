//
//  MovieListViewModel.swift
//  MovieDemo
//
//  Created by Jazlly on 07/06/2021.
//

import UIKit

class MovieListViewModel {

    var changeHandler: ((BaseViewModelChange) -> Void)?
    let networkServiceCall = NetworkServiceCalls()
    var onSelection: ((MoviewResults) -> ())?

    var moviewResults: [MoviewResults] = [] {
        didSet {
            self.changeHandler?(.updateDataModel)
        }
    }

    func startSynching() {
        emit(.loaderStart)
        self.networkServiceCall.getPopularMovieList { (state) in
            self.emit(.loaderEnd)
            self.moviewResults = LocalData.shared.getMovieList()?.results ?? []

            switch state {
            case .success( let result as GetMovieDataResponse):
                
                LocalData.shared.setMoviewLists(result)
                self.moviewResults = LocalData.shared.getMovieList()?.results ?? []
                self.emit(.updateDataModel)

            case .failure(let error):
                self.emit(.loaderEnd)
                self.emit(.error(message: error))
                
            default:break
            }
        }
    }
    
    func emit(_ change: BaseViewModelChange) {
             changeHandler?(change)
    }
    
    // MARK:-   Configure TableView -
    var numberOfSections: Int {
        return self.moviewResults.count
    }
    func getTableViewCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let movieViewCell = tableView.dequeueReusableCell(withIdentifier: MovieViewCell.reuseIdentifier, for: indexPath) as? MovieViewCell
        else {
            return UITableViewCell()
        }
        movieViewCell.movideData = self.moviewResults[indexPath.row]

        return movieViewCell
        
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        
        if self.moviewResults.count > 0 {
            onSelection?(self.moviewResults[indexPath.row] )
        }
    }

}
