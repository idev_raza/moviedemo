//
//  MovieListViewController.swift
//  MovieDemo
//
//  Created by RK on 07/06/2021.
//

import UIKit

class MovieListViewController: BaseViewController,BaseViewControllerProtocol {
    
    @IBOutlet weak var mainTableView: UITableView!
    var viewModel : MovieListViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.bindViewModel()
        self.setupUI()
    }
    
    func bindViewModel() {
        self.viewModel = MovieListViewModel()

        self.viewModel?.changeHandler = {
            [unowned self] change in
            switch change {
            case .error(let errorMessage):
               // Error Pop-Up
                print(errorMessage)
                self.showAlert(errorMessage)
                break
            case .loaderEnd:
                self.removeSpinner()
                break
            case .loaderStart:
                self.showSpinner(onView: self.view)
                break
            case .updateDataModel:
                // BindData
                self.mainTableView.reloadData()
                break
            }
        }
        
        self.viewModel?.onSelection = { movieModel in
            let movieDetailVC = MovieDetailViewController.storyboardInstance()
            movieDetailVC.movieObject = movieModel
            self.navigationController?.pushViewController(movieDetailVC, animated: true)

        }

    }
    
    func setupUI() {
        self.viewModel?.startSynching()
        self.title = "Popular Movies"
        self.setTableView()
    }
    
    fileprivate func setTableView(){
        self.mainTableView.register(UINib(nibName: MovieViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: MovieViewCell.reuseIdentifier)
    }
    
}


extension MovieListViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int    {
        if self.viewModel?.numberOfSections ?? 0 > 0  {
            self.mainTableView.tableHeaderView = nil
        }
        else {
            var frame = self.mainTableView.frame
            frame.origin = CGPoint(x: 0,y :0)
            frame.size.height = 300
            let view = UIView(frame: frame)
            view.backgroundColor = UIColor.clear
            
            let margin = UIEdgeInsets(top: 100, left: 10, bottom: 100, right: 10)
            
            let label1 = UILabel(frame: CGRect(x: margin.left, y: margin.top, width: frame.width-(margin.left+margin.right), height: 50))
            label1.font = UIFont.systemFont(ofSize: 20)
            label1.numberOfLines = 0
            label1.textAlignment = .center
            label1.textColor = .lightGray
            label1.text = "No record found!"
            let label1NewSize = label1.sizeThatFits(CGSize(width: label1.frame.width, height: CGFloat.greatestFiniteMagnitude))
            frame.size.height = margin.top + label1NewSize.height + margin.bottom
            view.frame = frame
            label1.frame = CGRect(x: margin.left, y: margin.top, width: label1.frame.width, height: label1NewSize.height)
            
            view.addSubview(label1)
            self.mainTableView.tableHeaderView = view
        }
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.numberOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.viewModel?.getTableViewCell(tableView, cellForRowAt: indexPath) ?? UITableViewCell()
    }
    
}

extension MovieListViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.viewModel?.didSelectRow(at: indexPath)
    }
    
}
