//
//  BaseViewModel.swift
//  MovieDemo
//
//  Created by RK on 07/06/2021.
//

import UIKit

enum BaseViewModelChange {
    case loaderStart
    case loaderEnd
    case updateDataModel
    case error(message: String)

}
