//
//  BaseViewController.swift
//  MovieDemo
//
//  Created by RK on 07/06/2021.
//

import UIKit

protocol BaseViewControllerProtocol {
    func bindViewModel()
    func setupUI()
}

class BaseViewController: UIViewController {
    
    
    var loading : UIView?
    
    func showSpinner(onView : UIView) {
        let ai = UIActivityIndicatorView.init(style: .large)
        ai.startAnimating()
        ai.center = onView.center
        ai.tintColor = .red
        
        DispatchQueue.main.async {
            onView.addSubview(ai)
        }
        loading = ai
    }
    
    override public func viewDidLayoutSubviews() {
        self.loading?.center = self.view.center
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            self.loading?.removeFromSuperview()
            self.loading = nil
        }
    }
    
    //MARK:
    //MARK: Set Back Button Item
    func setBackBarItem(isNative: Bool) {
        
        self.navigationItem.hidesBackButton = true
        let button = UIButton.init(type: .custom)
        button.tintColor = .black
        button.setImage(UIImage.init(named: "back_icon"), for: UIControl.State.normal)
        button.addTarget(self, action:#selector(onClickBackBarItem), for: UIControl.Event.touchUpInside)

        button.frame = CGRect.init(x: 0, y: 0, width: 25, height: 25)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }

    @objc func onClickBackBarItem()
    {
        self.navigationController?.popViewController(animated: true)
    }

    
    func showAlert(_ message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
