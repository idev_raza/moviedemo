//
//  BaseViewCell.swift
//  MovieDemo
//
//  Created by Jazlly on 07/06/2021.
//

import UIKit

class BaseViewCell: UITableViewCell,ReuseIdentifiable{


}
protocol ReuseIdentifiable {
    static var reuseIdentifier: String { get }
}

extension ReuseIdentifiable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
