//
//  NetworkServiceCalls.swift
//  MovieDemo
//
//  Created by RK on 07/06/2021.
//

import UIKit

class NetworkServiceCalls: NetworkAdapter {
    
    
    //MARK:- Get Popular Movie  -
    func getPopularMovieList(_ completion: @escaping CompletionWithSuccessOrFailure) {
        let params = GetPopularMovieListParam()
        fetch(params, CommonParser<GetMovieDataResponse>.self) { (state) in
            switch state {
            case .success(let response):
                completion(.success(response: response))
            case .failure(let error):
                completion(.failure(error: error))
            }
        }
    }
    
    func getMovieDetails(movieID:Int, _ completion: @escaping CompletionWithSuccessOrFailure) {
        let params = GetMovieDetailsParam(movieId: movieID)
        fetch(params, CommonParser<GetMovieDetailsResponse>.self) { (state) in
            switch state {
            case .success(let response):
                completion(.success(response: response))
            case .failure(let error):
                completion(.failure(error: error))
            }
        }
    }
    

    
}
