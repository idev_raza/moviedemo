//
//  CommonParser.swift
//  MovieDemo
//
//  Created by RK on 07/06/2021.
//

import Foundation
import Alamofire


class CommonParser <T: Codable> : NSObject {
    
    var result : T?
    init(_ json: Parameters ) throws {
        if let results = json as Parameters? {
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: results, options: [])
                if let resultModel = try? JSONDecoder().decode(T.self, from: jsonData) {
                    self.result = resultModel
                }
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        } else {
            throw NetworkError.emptyResponse
        }
    }
    
}

extension CommonParser: NetworkParser {
    
    static func parse(_ json: Parameters) -> CompletionState {
        do {
            if let result = try CommonParser(json).result {
                                
                return .success(response: result)

            } else {
                return .failure(error: NetworkError.emptyResponse.localizedDescription)
            }
        } catch {
            return .failure(error: error.localizedDescription)
        }
    }
    
}
