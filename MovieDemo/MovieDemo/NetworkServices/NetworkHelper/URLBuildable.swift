//
//  URLBuildable.swift
//  MovieDemo
//
//  Created by RK on 07/06/2021.
//

import UIKit
import Foundation
import Alamofire

struct Constant {

    static let BASE_URL = "https://api.themoviedb.org/3/movie"
    static let API_KEY = "9511613094c96f652c83c4f51a48b055"
    static let IMAGE_KEY = "https://image.tmdb.org/t/p/w500/"
    static let MOVIE_LIST_DATA = "MOVIE_LIST_DATA"

}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json        = "application/json"
    case urlEncoded  = "application/x-www-form-urlencoded; charset=utf-8"
    case appleWallet = "application/vnd.apple.pkpass"
    case urlEncodedHttpBody  = "application/x-www-form-urlencoded"
    case textHtml = "text/html"
    case textJavascript = "text/javascript"

}

enum CompletionState {
    case success(response: Any)
    case failure(error: String)
}

enum NetworkError: Error {
    case emptyResponse
    case fullResponse
}



protocol URLBuildable: URLRequestConvertible {
    // HTTPMethod default is post
    var httpMethod: HTTPMethod {get}
    //API path for the request
    var path: String? {get}
    //Parameters
    var parameters: Parameters? {get}
    //Accept type in header
    var acceptType: ContentType {get}
    //Encoding
    var encoding: ParameterEncoding { get }
    
}



extension URLBuildable {
    // MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        var urlCopy = Constant.BASE_URL
        
        if let pathCopy = path {
            let removeWhiteSpace = pathCopy.components(separatedBy: .whitespaces).joined()
            urlCopy += removeWhiteSpace
        }
        print(urlCopy)
        let url = try urlCopy.asURL()
        var urlRequest = URLRequest(url: url)
        // HTTP Method
        urlRequest.httpMethod = httpMethod.rawValue
        // Common Headers
        urlRequest.addValue(acceptType.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        if let params = parameters {
            do {
//                printLog(Utility.printJsonText(object: params))
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        return urlRequest
    }
}
