//
//  GetMovieDetailsParam.swift
//  MovieDemo
//
//  Created by RK on 07/06/2021.
//

import UIKit
import Alamofire

class GetMovieDetailsParam: NSObject {

    var movieID :Int
    init(movieId :Int) {
        self.movieID = movieId
    }
    
}

extension GetMovieDetailsParam : URLBuildable {
    
    var parameters: Parameters? {
        return prepareParameters()
    }
    
    var path: String? {
        return "/\(self.movieID)?api_key=\(Constant.API_KEY)&language=en-US"
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var acceptType: ContentType {
        return .json
    }
    
    var encoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    func prepareParameters() -> Parameters? {
                
        return nil
    }
    
}
