//
//  GetMovieListParam.swift
//  MovieDemo
//
//  Created by Jazlly on 07/06/2021.
//

import UIKit
import Alamofire


class GetPopularMovieListParam {

}

extension GetPopularMovieListParam : URLBuildable {
    
    var parameters: Parameters? {
        return prepareParameters()
    }
    
    var path: String? {
        return "/popular?api_key=\(Constant.API_KEY)&language=en-US&page=1"
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var acceptType: ContentType {
        return .json
    }
    
    var encoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    func prepareParameters() -> Parameters? {
                
        return nil
    }
    
}
