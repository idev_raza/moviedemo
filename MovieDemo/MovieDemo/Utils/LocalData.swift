//
//  LocalData.swift
//  MovieDemo
//
//  Created by Jazlly on 07/06/2021.
//

import UIKit

class LocalData: NSObject {
    
    let userStandard = UserDefaults.standard;
    static let shared = LocalData()
    private override init()
    {
        
    }
    
    func setMoviewLists(_ getMovieDataResponse:GetMovieDataResponse)
    {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(getMovieDataResponse) {
            userStandard.set(encoded, forKey: Constant.MOVIE_LIST_DATA)
            userStandard.synchronize();
        }

    }
    
    func getMovieList() -> GetMovieDataResponse? {
       
        if let getMovieDataResponse = userStandard.object(forKey: Constant.MOVIE_LIST_DATA) as? Data {
            let decoder = JSONDecoder()
            if let getMovieDataResponsInfo = try? decoder.decode(GetMovieDataResponse.self, from: getMovieDataResponse) {
                return getMovieDataResponsInfo
            }
        }

        return nil
    }

    
    func setMoviewDetails(_ getMovieDetailsResponse:GetMovieDetailsResponse)
    {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(getMovieDetailsResponse) {
            userStandard.set(encoded, forKey: "\(getMovieDetailsResponse.id ?? 0)")
            userStandard.synchronize();
        }

    }
    

    func getMovieDetails(movieID: Int?) -> GetMovieDetailsResponse? {
       
        if let getMovieDataResponse = userStandard.object(forKey: "\(movieID ?? 0)") as? Data {
            let decoder = JSONDecoder()
            if let getMovieDetailResponsInfo = try? decoder.decode(GetMovieDetailsResponse.self, from: getMovieDataResponse) {
                return getMovieDetailResponsInfo
            }
        }

        return nil
    }



    
}
