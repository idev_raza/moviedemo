//
//  ImageUtility.swift
//  MovieDemo
//
//  Created by Jazlly on 07/06/2021.
//

import UIKit
import  ImageIO
import SDWebImage

extension UIImageView
{
    func downloadImageFrom(link: String,
                        placeHolder:String = "placeholder",
                        isFromCache:Bool = true,
                        isIndicator:Bool = false,
                        mode:UIView.ContentMode = .scaleAspectFill,isAppendBaseUrl:Bool = true) {
        
        self.contentMode = mode
        self.clipsToBounds = true;
        let placeHolderImage = UIImage.init(named: placeHolder)
        self.image=placeHolderImage;
        
        if link.isEmpty {
            return
        } else {
            
            let urlStr = Constant.IMAGE_KEY + link
            let url = URL(string: urlStr)
            let placeHolderImage = UIImage(named: placeHolder)
        
            self.sd_imageTransition = .fade
            self.sd_imageIndicator = SDWebImageActivityIndicator.medium
            self.sd_setImage(with: url, placeholderImage: placeHolderImage)
        }
    }
    
    func downloadCroppedImageForItems(link: String,
                        placeHolder:String = "placeholder",
                        isFromCache:Bool = true,
                        isIndicator:Bool = false,
                        mode:UIView.ContentMode = .scaleAspectFill,isAppendBaseUrl:Bool = true) {
        
        self.contentMode = mode
        self.clipsToBounds = true;
        let placeHolderImage = UIImage.init(named: placeHolder)
        self.image=placeHolderImage;
        
        if link.isEmpty {
            return
        } else {
            let urlStr = Constant.IMAGE_KEY + link
            let url = URL(string: urlStr)
            let placeHolderImage = UIImage.init(named: placeHolder)
        
            self.sd_imageTransition = .fade
            self.sd_imageIndicator = SDWebImageActivityIndicator.medium
            let thumbnailSize = CGSize(width: 350, height: 280) // Thumbnail will bounds to (200,200)
            self.sd_setImage(with: url, placeholderImage: placeHolderImage, options: [.scaleDownLargeImages], context: [.imageThumbnailPixelSize : thumbnailSize])
        }
    }
 
        
    func downloadedImageIfLoadImageOnBlackAndWhite(link: String,
                                      placeHolder:String = "placeholder",
                                      isFromCache:Bool = true,
                                      isIndicator:Bool = false,
                                      mode:UIView.ContentMode = .scaleAspectFill,isAppendBaseUrl:Bool = true) {
        self.contentMode = mode
        self.clipsToBounds = true;
        let placeHolderImage = UIImage.init(named: placeHolder)
        self.image=placeHolderImage;
        
        if link.isEmpty {
            return
        } else {
            let urlStr = Constant.IMAGE_KEY + link
            let url = URL(string: urlStr)
            let placeHolderImage = UIImage.init(named: placeHolder)
            self.image = placeHolderImage
            let thumbnailSize = CGSize(width: 100, height: 100)
            SDWebImageManager.shared.loadImage(with: url, options: .highPriority, context: [.imageThumbnailPixelSize : thumbnailSize], progress: nil) { (image, data, error, nil, completed, url) in
                if let downloadImage = image {
                    DispatchQueue.global(qos: .background).async {
                        let context = CIContext(options: nil)
                        let currentFilter = CIFilter(name: "CIPhotoEffectNoir")
                        currentFilter!.setValue(CIImage(image: downloadImage), forKey: kCIInputImageKey)
                        let output = currentFilter!.outputImage
                        let cgimg = context.createCGImage(output!,from: output!.extent)
                        let processedImage = UIImage(cgImage: cgimg!)
                        DispatchQueue.main.async {
                            self.image = processedImage
                        }
                    }
                }
            }
        }
    }
    
}


extension UIView {
    
    func  setRound(withBorderColor:UIColor=UIColor.clear, andCornerRadious:CGFloat = 0.0, borderWidth:CGFloat = 1.0)
    {
        if andCornerRadious==0.0
        {
            var frame:CGRect = self.frame;
            frame.size.height=min(self.frame.size.width, self.frame.size.height) ;
            frame.size.width=frame.size.height;
            self.frame=frame;
            self.layer.cornerRadius=self.layer.frame.size.width/2;
        }
        else
        {
            self.layer.cornerRadius=andCornerRadious;
        }
        self.layer.borderWidth = borderWidth
        self.clipsToBounds = true;
        self.layer.borderColor = withBorderColor.cgColor
    }

}
